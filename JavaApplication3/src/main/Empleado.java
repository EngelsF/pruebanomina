/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author Sistemas33
 */
public class Empleado {
    private String id;
    private String carnet;
    private String nombres;
    private String apellidos;
    private String cedula;
    private String direccion;
    private String telefono;
    private String salariohora;
    private String salarioneto;

    public Empleado() {
    }

    public Empleado(String id, String carnet, String nombres, String apellidos, String cedula, String direccion, String telefono, String salariohora, String salarioneto) {
        this.id = id;
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salariohora = salariohora;
        this.salarioneto = salarioneto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSalariohora() {
        return salariohora;
    }

    public void setSalariohora(String salariohora) {
        this.salariohora = salariohora;
    }

    public String getSalarioneto() {
        return salarioneto;
    }

    public void setSalarioneto(String salarioneto) {
        this.salarioneto = salarioneto;
    }
    
    
}
